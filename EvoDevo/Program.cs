﻿using System;
using System.IO;
using System.Linq;
using System.CodeDom.Compiler;
using System.Text;
using System.Threading;
using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;

namespace EvoDevo
{
	internal static class Program
	{
		private const int R1 = 2;
		private const int R2 = 3;
		private static readonly Random R = new Random();

		public static void Main(string[] a)
		{
			Directory.CreateDirectory("Out");

			// getting the code
			var dna = a.Any() ? a[0] : File.ReadAllText(@"..\..\Program.cs");

			// evolution loop
			for (var i = 0; i < 3000; i++)
			{
				var r = false;
				try
				{
					r = Cr(Randomize(dna));
				}
				catch
				{
					// ignored
				}

				if (!r)
					continue;

				var file =
					@"Out\id_" + Thread.CurrentThread.ManagedThreadId +
					"_index_" + i + ".cs";

				File.WriteAllText(file, dna);
			}
		}

		private static string Randomize(string dnaString)
		{
			var sb = new StringBuilder(dnaString);
			for (var i = 0; i < R.Next(R1, R2); i++)
			{
				var randChar = R.Next(sb.Length);
				sb[randChar] = (char) R.Next(255);
			}

			return sb.ToString();
		}

		private static bool Cr(string code)
		{
			var compilerParams = new CompilerParameters
			{
				GenerateInMemory = true,
				TreatWarningsAsErrors = false,
				GenerateExecutable = false,
				CompilerOptions = "/optimize",
				IncludeDebugInformation = false,
				ReferencedAssemblies =
				{
					"mscorlib.dll",
					"System.dll",
					"System.Data.dll",
					"System.Core.dll",
					@"C:\Users\gidch\Desktop\EvoDevo\packages\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.2.0.1\lib\net45\Microsoft.CodeDom.Providers.DotNetCompilerPlatform.dll",
					@"C:\Program Files (x86)\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.7.2\Microsoft.CSharp.dll"
				}
			};

			var c = new CSharpCodeProvider().CompileAssemblyFromSource(compilerParams, code);
			if (c.Errors.HasErrors)
				return false;

			Console.WriteLine("compile successful " + Thread.CurrentThread.ManagedThreadId);

			var m = c.CompiledAssembly.GetModules().First();
			var mt = m?.GetType("EvoDevo.Program");
			var mi = mt?.GetMethod("Main");

			if (mi == null)
				return false;

			var t = new Thread(() => mi.Invoke(null, new object[] {new[] {code}}));
			t.Start();
			return true;
		}
	}
}